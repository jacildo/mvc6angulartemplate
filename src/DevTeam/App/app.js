﻿(function () {

    'use strict';

    angular.module('app', ['ngRoute', 'booksServices'])
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/Views/Default/index.html'
            })
            .when('/about', {
                templateUrl: '/Views/Default/about.html'
            })
            .when('/books', {
                templateUrl: '/Views/Books/index.html',
                controller: 'booksController'
            })
            .otherwise({
                redirectTo: '/'
            })

        $locationProvider.html5Mode(true);
    }
})();