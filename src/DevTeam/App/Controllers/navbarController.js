﻿(function () {

    navbarController.$inject = ['$scope', '$http'];

    function navbarController($scope, $http) {
        $scope.message = "Hello from the other side";
    }

    angular.module('app')
        .controller('navbarController', navbarController)
        .directive('navbar', function () {
            return {
                restrict: 'E',
                templateUrl: '/Views/Partials/navbar.html',
                controller: 'navbarController'
            }
        });
}());
