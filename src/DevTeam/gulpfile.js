/// <binding BeforeBuild='build' Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

var paths = {
    webroot: "./wwwroot/"
};

var dependencies = {
    './node_modules/angular/angular*.js': paths.webroot + 'lib/angular',
    './node_modules/angular-resource/angular-resource*.js': paths.webroot + 'lib/angular-resource',
    './node_modules/angular-route/angular-route*.js': paths.webroot + 'lib/angular-route',
}


paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);

// copy listed dependencies into library folder
gulp.task("build:dependencies", function () {
    for (var src in dependencies) {
        if (!dependencies.hasOwnProperty(src)) continue;
        gulp.src(src)
            .pipe(gulp.dest(dependencies[src]));
    }
});

// compiles and minifies app module, controllers, and services (in order) into app.js
gulp.task("build:app", function () {
    return gulp.src(['./App/app.js', './App/Controllers/*.js', './App/Services/*.js'])
        .pipe(concat(paths.webroot + 'app.js'))
        .pipe(uglify({ mangle: false }))
        .pipe(gulp.dest("."));
});

// move all views to Views folder in wwwroot
gulp.task("build:views", function () {
    return gulp.src('./App/Views/**')
        .pipe(gulp.dest(paths.webroot + "Views"));
});

gulp.task("build", ['build:dependencies', 'build:views', 'build:app']);

gulp.task("watch", function () {
    return gulp.watch('App/**', ['build']);
});